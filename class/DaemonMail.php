<?php
include("XmlMailer.php");

class DaemonMail extends XmlMailer {
	
    private $fileMails = array();
	private $pidfile = "/var/run/xmlmailer.pid";
	
	function initDaemon()
	{
		$pid = pcntl_fork();
		if($pid == -1)
			die("somethin was wrong with process forking!\n");
			
		if($pid) {
			$f = fopen($this->pidfile,"w");
			if ($f) {
				fwrite($f,$pid);
				fclose($f);
			}
			exit;
		}

		if (!posix_setsid()) {
			die ("can't leave the terminal launcher\n");
		}

		chdir("/");
		umask(0);

		for(;;sleep(1)) {
    		$this->fileMails = $this->repositoryCheck();
    		foreach ($this->fileMails as $xmldoc) {
    			$this->sendlog->write("-----begin to parse ".$xmldoc."-----");
    			if (!$this->parseXmlMail($xmldoc)) {
    				$this->sendlog->write("-----end because no load ".$xmldoc."-----");
    				continue;
    			}
    			$this->movefiles($xmldoc);
    			$this->sendlog->write("-----end parse of ".$xmldoc."-----");
    		}
		}
		$this->log->write("======= DAEMON FINISHED =======");
		exit("Daemon finished...\n");
	}
	
	public function repositoryCheck()
	{ 
		$lineas = array();
		$dir = opendir($this->basedir."/".$this->xmldir);
		while($linea = readdir($dir))
		{
		      $ultimo = substr($linea,-3);
		      if($ultimo == "xml")
		              $lineas[] = $linea;
		}
		closedir($dir);
		return $lineas;
	}
	

} //end of class

?>