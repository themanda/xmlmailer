<?php
include("class.smtp.php");
include("class.phpmailer.php");
include("Log.php");

class XmlMailer {
       
	protected $xmlFile    = NULL;
    protected $host       = "";
    protected $service    = "smtp";
    protected $basedir    = "mailer";
    protected $attachdir  = "files";
    protected $xmldir     = "xml";
    protected $xmldone    = "enviados";
    protected $attachdone = "archivos";
    protected $noticeaddr = "sys@adem.cl";
    protected $noticealt  = "sistema de confirmacion";
    protected $logdir     = "log";
    protected $logname	= false;
    protected $log		= false;
	protected $sendlog	= false;
	
    function __construct($cfg)
    {
		$this->XmlMailer($cfg);
    }
    
    function XmlMailer($cfg = array()) 
    {
		$this->initialize($cfg);
    	$this->directorybuild();
    	$this->sendlog = new Log($this->basedir."/".$this->logdir."/send.log");
		if($this->logname != false) {
			$this->logname = $this->basedir."/".$this->logdir."/".$this->logname;
			$this->log = new Log($this->logname);
		}
    }

    protected function initialize($args = array())
    {
		foreach ($args as $key => $val)
	    	if (isset($this->$key))
				$this->$key = $val;
    }
    
    public function parseXmlMail($xml)
    {   
		$r = "";	
		$this->xmlFile = $xml;
	    $mail   = new phpmailer();
		$xmlDoc = new DOMDocument();
		
		$mail->Host     = $this->host;
		$mail->Mailer   = $this->service;
	
		
		if (!$xmlDoc->load($this->basedir."/".$this->xmldir."/".$this->xmlFile)) {
			$this->log->write("Error: trying load xml ".$this->xmlFile);
			return false;
		}
		$nodes = $xmlDoc->getElementsByTagName("mail");
	
		foreach ($nodes as $node) {
		    if ($node->hasChildNodes()) {
				$note = false;
				foreach ($node->childNodes as $child) {
					if( $child->nodeName == "from") {
						$mail->From = $child->nodeValue;
						$mail->FromName = $child->nodeValue;
						if ($child->hasAttributes() && $child->getAttribute("acuse") == "true") {
							$note = true;
							$notifica['to'] =  $child->nodeValue;
						}
					}
					if( $child->nodeName == "subject") {
						$mail->Subject = $child->nodeValue;
						if ($note) $notifica['subject'] =  $child->nodeValue;
					}			
					if( $child->nodeName == "body")
						$mail->Body = $child->nodeValue;
				}
				foreach ($node->getElementsByTagName("attachment") as $son) {
					if ($son->parentNode->hasChildNodes()) {
						foreach ($son->getElementsByTagName("file") as $to) {
							$r = $mail->AddAttachment($this->basedir."/".$this->attachdir."/".$to->nodeValue, $to->nodeValue);
							$r?$this->sendlog->write($to->nodeValue." has been attached"):$this->sendlog->write($to->nodeValue." will not been attached");
						}
					}
				}
				foreach ($node->getElementsByTagName("destination") as $son) {
					if ($son->parentNode->hasChildNodes()) {
						foreach ($son->getElementsByTagName("to") as $to) {
							$mail->AddAddress($to->nodeValue);
							$this->sendlog->write("to: ".$to->nodeValue);
						}
						foreach ($son->getElementsByTagName("cc") as $cc) {
							$mail->AddCC($cc->nodeValue);
							$this->sendlog->write("cc: ".$cc->nodeValue);
						}	
						foreach ($son->getElementsByTagName("bcc") as $bcc) {
							$mail->AddBCC($bcc->nodeValue);
							$this->sendlog->write("bcc: ".$bcc->nodeValue);
						}						
					}
				}
				if ($note)@$this->notificar(&$notifica);
				if(!$mail->Send()) {
	        		$this->log->write("Error Can't send mail");
	        		$this->sendlog->write("Error: send Problem maybe destination account don't exist'");
				}
				else
					$this->sendlog->write("email has been sended to all destinations");
				$mail->ClearAddresses();
				$mail->ClearCCs();
				$mail->ClearBCCs();
				$mail->ClearAttachments();
			}
		}
		return true;
    }

	protected function notificar($data = array())
	{
		$mail = new phpmailer();
		$mail->Host     = $this->host;
		$mail->Mailer   = $this->service;
		$mail->From 	= $this->noticeaddr;
		$mail->FromName = $this->noticealt;
		$mail->Subject 	= "Notificación:".$data['subject']." fue envieado correctamente";
		$mail->Body 	= "Sistema de avisos No responder este E-Mail";
		$mail->AddAddress($data['to']);
		if(!$mail->Send())
			$this->log->write("Error: no send notification to ".$data['to']);
		$mail->ClearAddresses();
	}

	public function movefiles($file)
	{
		$xmlDoc = new DOMDocument();
		$xmlDoc->load($this->basedir."/".$this->xmldir."/".$file);

		$nodes = $xmlDoc->getElementsByTagName("mail");

		foreach ($nodes as $node)
		{
		    if ($node->hasChildNodes()) {
                	foreach ($node->getElementsByTagName("attachment") as $son)
                	{
                        	if ($son->parentNode->hasChildNodes()) {
                                	foreach ($son->getElementsByTagName("file") as $to) 
                                        	if(!$this->mover($this->basedir."/".$this->attachdir."/".$to->nodeValue,$this->basedir."/".$this->attachdone."/".$to->nodeValue))
							$this->log->write("Error Can't move file: ".$to->nodeValue);
                        	}
                	}
		    }
		}
		if(!$this->mover($this->basedir."/".$this->xmldir."/".$file,$this->basedir."/".$this->xmldone."/".$file))
			$this->log->write("Error Can't move file: ".$file);
	}

	protected function mover($src,$des)
	{
		if (!file_exists($src)){
			$this->log->write("Error file ".$src."don't exist");
			return false;
		}
		if(!copy($src,$des))
			return false;
		else 
		  return unlink($src);
	}
	
	private function directorybuild() {
		if (!file_exists($this->basedir) ) {
			if (mkdir($this->basedir,0777,true)) {
				mkdir($this->basedir."/".$this->attachdir,0777,true);
				mkdir($this->basedir."/".$this->xmldir,0777,true);
				mkdir($this->basedir."/".$this->attachdone,0777,true);
				mkdir($this->basedir."/".$this->xmldone,0777,true);
				mkdir($this->basedir."/".$this->logdir,0777,true);
			}
			else
				echo "Error: Directory permission denied \n";
		}
	}
	
} // end of class
