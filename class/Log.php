<?
class Log {

	private $fileLog;
 	private $fileHandler = FALSE;
	private $maxsize = 2097152;
	function __construct($log = NULL)
	{
		$this->fileLog = $log;
	}

	function Log($log)
	{
		__construct($log);
	}

	private function open()
	{
		$this->fileHandler = fopen($this->fileLog,"a");
		if (!$this->fileHandler) {
			echo "Error can't open ".$this->fileLog."\n";
			return FALSE;
		}
		return TRUE;
	}

	public function write($msg)
	{
		$this->rotate();
		if($this->open()) {
			if (fwrite($this->fileHandler,$this->getTime()." ".$msg."\n") == FALSE) {
				echo "Error Can't write ".$this->fileLog."\n";
				return FALSE;
			}
			if (!$this->close()) {
				echo "Error Can't close ".$this->fileLog."\n";
				return FALSE;
			}
			return TRUE;
		}
		else {
			echo "Error Can't Open ".$this->fileLog."\n";
			return FALSE;
		}
	}

	private function close()
	{
		return fclose($this->fileHandler);
	}

	private function getTime()
	{
		return date("m d H:i:s");
	}
	
	private function rotate()
	{
		if (filesize($this->fileLog) >= $this->maxsize ){
			rename($this->fileLog,$this->fileLog.".old");
		}
	}
}

?>
