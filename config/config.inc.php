<?php

global $cfg;

$cfg['host']		= "mail.server.com";
$cfg['service']		= "smtp";

$cfg['basedir']		= "/path/to/mails";
$cfg['attachdir']	= "files_dir";
$cfg['xmldir']		= "xml_dir";
$cfg['xmldone']		= "sent";
$cfg['attachdone']	= "achived";
$cfg['noticeaddr']	= "sender@mail.here";
$cfg['noticealt']	= "subject text";

$cfg['logdir']		= "log_dir";
$cfg['logname']		= "xmlmailer.log";

?>
